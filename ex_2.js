/*
Đầu vào: 
5 số thực a, b, c, d, e (3,6,4,2,11)

Xử lí dữ liệu:
- Bước 1: Khai báo biến a, b, c, d, e đại diện cho các số thực, biến giaTriTrungBinh
- Bước 2: Tính theo công thức giaTriTrungBinh = (a + b + C + d + e)/5
- Bước 3: In kết quả ra consoloe

Đầu ra:
Giá trị trung bình của 5 số thực
*/

var a = 3;
var b = 6;
var c = 4;
var d = 2;
var e = 11;
var giaTriTrungBinh = 0;

giaTriTrungBinh = (a + b + c + d + e)/5;

console.log("Gia Tri Trung Binh =", giaTriTrungBinh);