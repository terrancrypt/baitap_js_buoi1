/*
Đầu vào: 
Số tiền USD (231$), Tỷ giá USD quy đổi sang VND (23.500VNĐ/1 USD) 

Xử lí dữ liệu:
- Bước 1: Khai báo biến USD (231), tyGia (23500), VND
- Bước 2: Gán giá trị vào biến USD
- Bước 3: Tính theo công thức VND = tyGia * USD;
- Bước 4: In kết quả ra console

Đầu ra:
Số tiền sau quy đổi bằng VND.
*/

var USD = 231;
var tyGia = 23500;
var VND = 0;

VND = tyGia * USD;

console.log("Số tiền quy đổi =", VND, "VND");