/*
Đầu vào:
Số có 2 chữ số (85)

Xử lí dữ liệu:
- Bước 1: Khai báo các biến so, tongKySo, hangDonVi, hangChuc
- Bước 2: Gán giá trị cho biến so
- Bước 3: Tính để lấy số hàng đơn vị và hàng chục theo công thức: hangDonVi = so % 10 ; hangChuc = so / 10
- Bước 4: Tính tổng ký số theo công thức: tongKySo = hangDonVi + hangChuc
- Bước 5: In kết quả ra console

Đầu ra: 
Tổng của 2 ký số của số đã nhập
*/ 

var so = 85;
var tongKySo;

var hangDonVi = Math.floor(so % 10);
var hangChuc = Math.floor(so / 10);

tongKySo = hangDonVi + hangChuc;

console.log("Tổng ký số =", tongKySo);
