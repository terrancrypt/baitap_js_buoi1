/*
Đầu vào: 
Chiều dài (12cm), chiều rộng (6cm) của hình chữ nhật.

Xử lí dữ liệu:
- Bước 1: Khai báo biến chieuDai, chieuRong, dienTich, chuVi
- Bước 2: Gán gán trị cho chieuDai và chieuRong
- Bước 3: Tính theo công thức: dienTich = chieuDai * chieuRong ; chuVi = (chieuDai + chieuRong) * 2
- Bước 4: In kết quả ra console

Đầu ra:
Diện tích và chu vi của hình chữ nhật
*/ 

var chieuDai = 12;
var chieuRong = 6;
var dienTich = 0;
var chuVi = 0;

dienTich = chieuDai * chieuRong;
chuVi = (chieuDai + chieuRong) * 2;

console.log("Diện tích của hình chữ nhật = ", dienTich);
console.log("Chu vi của hình chữ nhật = ", chuVi);
