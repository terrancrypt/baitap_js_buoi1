/*Đầu vào:
Số tiền lương nhân viên (100.000đ), Số ngày làm việc của nhân viên

Xử lí dữ liệu:
- Bước 1: Khai báo biến tienLuong1Ngay (100000), soNgayLamViec (21 ngay), tongSoTienLuong
- Bước 2: Gán giá trị cho bien soNgayLamViec
- Bước 3: Tính theo công thức tongSoTienLuong = tienLuong1Ngay * soNgayLamViec
- Bước 4: In kết quả ra console

Đầu ra:
Kết quả tiền lương nhân viên đã làm được
*/

var tienLuong1Ngay  = 100000;
var soNgayLamViec = 21;
var tongSoTienLuong = 0;

tongSoTienLuong = tienLuong1Ngay * soNgayLamViec;

console.log("Tổng số tiền lương =",tongSoTienLuong);
